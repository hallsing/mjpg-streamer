#!/bin/bash
path=/home/pi/mjpg-streamer

declare -i port
declare -a video
declare -a pid_of_video

port=8000

#if [ $# != "3" ]; then
 #  echo "arg error"
  # exit 0
#fi


video=(`ls /dev | grep video`)
count=${#video[*]}
for (( i=0; i<$count; i=i+1,port=port+1 ))
do

  $path/mjpg_streamer -i "$path/input_uvc.so -d /dev/${video[i]} -n -q $1 -y -f $2 -r $3" -o "$path/output_http.so -n -w $path/webcam -p $port" &
  video[i]="$!"

  echo $port
  sleep 1

done

